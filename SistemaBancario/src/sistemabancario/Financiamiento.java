package sistemabancario;

public abstract class Financiamiento {
    //atributos
    private Personas_Juridicas clienteJuridico;
    private Personas_Naturales clienteNatural;
    //contructores
    public Financiamiento(Personas_Juridicas clienteJuridico){
     this.clienteJuridico=clienteJuridico;
      this.clienteNatural=null;
    }
    public Financiamiento(Personas_Naturales clienteNatural){
     this.clienteNatural=clienteNatural;
     this.clienteJuridico=null;
    }
    //metodos
    public Object getClienteJuridico() {
        return clienteJuridico==null ? mensajes.CLIENTENOEXISTE : clienteJuridico ;
    }

    public void setClienteJuridico(Personas_Juridicas clienteJuridico) {
        this.clienteJuridico = clienteJuridico;
    }

    public Object getClienteNatural() {
        return clienteNatural==null ? mensajes.CLIENTENOEXISTE :  clienteNatural;
    }

    public void setClienteNatural(Personas_Naturales clienteNatural) {
        this.clienteNatural = clienteNatural;
    }
    
    public double getImpuestos(){
        return getBeneficio() * 0.001;
    }
    public abstract double getBeneficio();
    
   
    
}
