package sistemabancario;

public class Personas_Juridicas extends Personas{
    //Atributos
    private int numeroSocios;
    private String nombreRepresentanteLegal;
    private String fechaFundacion;
    
    //Construcctores
    public Personas_Juridicas(String ci_O_ruc, String nombre, String direccion,int numeroSocios, String nombreRepresentanteLegal, String fechaFundacion) {
        super(ci_O_ruc, nombre, direccion);
        this.numeroSocios = numeroSocios;
        this.nombreRepresentanteLegal = nombreRepresentanteLegal;
        this.fechaFundacion = fechaFundacion;
    }

    public Personas_Juridicas() {
        super();
    }
    
    //Metodos
    public int getNumeroSocios() {
        return numeroSocios;
    }

    public String getNombreRepresentanteLegal() {
        return nombreRepresentanteLegal;
    }

    public String getFechaFundacion() {
        return fechaFundacion;
    }

    public void setNumeroSocios(int numeroSocios) {
        this.numeroSocios = numeroSocios;
    }

    public void setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
        this.nombreRepresentanteLegal = nombreRepresentanteLegal;
    }

    public void setFechaFundacion(String fechaFundacion) {
        this.fechaFundacion = fechaFundacion;
    }
    
    public void presentacionDatos(){
        String datos="";
        datos+="Nombre: "+this.nombre+"\n";
        datos+="RUC: "+this.ci_O_ruc+"\n";
        datos+="Direccion: "+this.direccion+"\n";
        datos+="Numero de Socios: "+this.numeroSocios+"\n";
        datos+="Nombre del representante Legar: "+this.nombreRepresentanteLegal+"\n";
        datos+="Fecha de Fundacion: "+this.fechaFundacion+"\n";
        System.out.println(datos);
    }
    
    
    
    
    
}
