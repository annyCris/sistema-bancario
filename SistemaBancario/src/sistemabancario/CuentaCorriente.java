package sistemabancario;

public class CuentaCorriente extends Cuenta{
    //Atrubutos
    private int serieChequera;
    
    //Constructores
    public CuentaCorriente(long numCuenta, Personas_Juridicas titularPJ, double saldoActual,int serieChequera) {
        super(numCuenta, titularPJ, saldoActual);
        this.serieChequera = serieChequera;
    }

    public CuentaCorriente(long numCuenta, Personas_Naturales titularPN, double saldoActual,int serieChequera) {
        super(numCuenta, titularPN, saldoActual);
        this.serieChequera = serieChequera;
    }

    public CuentaCorriente() {
        
    }
    
    //Metodos
    public int getSerieChequera() {
        return serieChequera;
    }

    public void setSerieChequera(int serieChequera) {
        this.serieChequera = serieChequera;
    }

    public String reintegro(double saldoAbonado, String tipoPersona) {
        String datos="";
        datos+=super.reintegro(saldoAbonado, tipoPersona);
        datos+="La serie de su chequera es "+this.serieChequera;
        return datos; //To change body of generated methods, choose Tools | Templates.
    }

    public String ingresos(double saldoAbonado, String tipoPersona) {
        String datos="";
        datos+=super.ingresos(saldoAbonado, tipoPersona);
        datos+="La serie de su chequera es "+this.serieChequera;
        return datos; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
    
    
}
