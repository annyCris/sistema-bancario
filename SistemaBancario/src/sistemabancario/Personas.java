package sistemabancario;

public class Personas {
    //Atributos
    public String ci_O_ruc;
    public String nombre;
    public String direccion;
    
    //Constructores
    public Personas(String ci_O_ruc, String nombre, String direccion) {
        this.ci_O_ruc = ci_O_ruc;
        this.nombre = nombre;
        this.direccion = direccion;
    }
        
    public Personas() {
    }
    
    //Getters y Setters
    public String getCi_O_ruc() {
        return ci_O_ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setCi_O_ruc(String ci_O_ruc) {
        this.ci_O_ruc = ci_O_ruc;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
