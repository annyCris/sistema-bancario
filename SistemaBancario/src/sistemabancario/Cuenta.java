package sistemabancario;

public class Cuenta {
    //Atributos
    protected long numCuenta;
    protected Personas_Juridicas titularPJ;
    protected Personas_Naturales titularPN;
    protected double saldoActual;
    
    //Constructores
    public Cuenta(long numCuenta, Personas_Juridicas titularPJ, double saldoActual) {
        this.numCuenta = numCuenta;
        this.titularPJ = titularPJ;
        this.saldoActual = saldoActual;
    }
    public Cuenta(long numCuenta, Personas_Naturales titularPN, double saldoActual) {
        this.numCuenta = numCuenta;
        this.titularPN = titularPN;
        this.saldoActual = saldoActual;
    }

    public Cuenta() {
    }
    
    //Metodos
    public Personas_Juridicas getTitularPJ() {
        return titularPJ;
    }

    public Personas_Naturales getTitularPN() {
        return titularPN;
    }
    
    public long getNumCuenta() {
        return numCuenta;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setNumCuenta(long numCuenta) {
        this.numCuenta = numCuenta;
    }

    public void setTitularPJ(Personas_Juridicas titularPJ) {
        this.titularPJ = titularPJ;
    }

    public void setTitularPN(Personas_Naturales titularPN) {
        this.titularPN = titularPN;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }
    
    public String reintegro(double saldoAbonado,String tipoPersona){
        String datos="";
        switch(tipoPersona){
            case "Natural": 
                datos+="\n El numero de cuenta es: "+this.numCuenta+"\n";
                datos+=" El saldo actual de la cuenta es: "+this.saldoActual+"\n";
                datos+=" Titular de la cuenta "+this.titularPN.nombre+"\n";
                break;

            case "Juridica": 
                datos+="\nEl numero de cuenta es: "+this.numCuenta+"\n";
                datos+="El saldo actual de la cuenta es: "+this.saldoActual+"\n";
                datos+="Titular de la cuenta "+this.titularPJ.nombre+"\n";
                break;
            
            default:datos+="No existe ese tipo de persona";
            break;
            
        }
        if (saldoAbonado > saldoActual*0.25){
              datos+=mensajes.ADVERTENCIATRANSAC.toString() +" "+saldoActual*0.25+" como maximo";
              return datos;
        }
        datos+=" Por favor espere "+mensajes.TRANSACCIONPROGRESO.toString();
        return datos;
    }
    public String ingresos(double saldoAbonado,String tipoPersona){
        String datos="";
        if(tipoPersona=="Natural"){ 
            datos+="\n El numero de cuenta es: "+this.numCuenta+"\n";
            datos+=" El saldo actual de la cuenta es: "+this.saldoActual+"\n";
            datos+=" Titular de la cuenta "+this.titularPN.nombre+"\n";        
        }else if (tipoPersona=="Juridica") {
            datos+="\nEl numero de cuenta es: "+this.numCuenta+"\n";
            datos+="El saldo actual de la cuenta es: "+this.saldoActual+"\n";
            datos+="Titular de la cuenta "+this.titularPJ.nombre+"\n"; 
        }
        saldoActual= saldoAbonado+saldoActual;
        datos+="Estado de la transaccion "+mensajes.TRANSACCIONCOMPLETA.toString()+"\n";
        return datos;
    }
}
