/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemabancario;

/**
 *
 * @author idenEclips
 */
public class DepositoEstrcturado extends Deposito implements Amortizable{
    //atributos
    double capitalVariab;
    double interesVariab;
    
    //constructores

    public DepositoEstrcturado(Personas_Naturales clienteNatural, double capitalActual, float tiempoAniosLiquidez, int timepoDias, double interesRentabilidad) {
        super(clienteNatural, capitalActual, tiempoAniosLiquidez, timepoDias, interesRentabilidad);
    }

    public DepositoEstrcturado(Personas_Juridicas clienteJuridico, double capitalActual, float tiempoAniosLiquidez, int timepoDias, double interesRentabilidad) {
        super(clienteJuridico, capitalActual, tiempoAniosLiquidez, timepoDias, interesRentabilidad);
    }

    public DepositoEstrcturado(Personas_Naturales clienteNatural, double capitalVariab, double interesVariab, Deposito Cliente) {
        super(clienteNatural,Cliente);
        this.capitalVariab = capitalVariab;
        this.interesVariab = interesVariab;
    }

    public DepositoEstrcturado(Personas_Juridicas clienteJuridico, double capitalVariab, double interesVariab, Deposito Cliente) {
        super(clienteJuridico, Cliente);
        this.capitalVariab = capitalVariab;
        this.interesVariab = interesVariab;
    }
    
    //metodos
    public double getCapitalVariab() {
        return capitalVariab;
    }

    public void setCapitalVariab(double capitalVariab) {
        this.capitalVariab = capitalVariab;
    }

    public double getInteresVariab() {
        return interesVariab*capitalVariab*getTiempoAniosLiquidez();
    }

    public void setInteresVariab(double interesVariab) {
        this.interesVariab = interesVariab;
    }
    //metodos de la interfaz
    @Override
    public String toString() {
        return 
                "Capital variable " + capitalVariab + 
                "\nInteres variable " + interesVariab + 
                "\nBeneficios del capital variable " + getInteresVariab();
    }

    @Override
    public boolean amortizar(double cantidad) {
         if (capitalVariab > cantidad){
             capitalVariab = capitalVariab - cantidad;
             return true;
         }
         return false;
    }
    
    
    

}
