package sistemabancario;

public class Deposito extends Financiamiento{
    // atributos
    protected double capitalActual;
    protected float tiempoAniosLiquidez;
    protected int timepoDias;
    private final double capitalBase = 3000.00;
    protected double interesRentabilidad;
    
    //constructores  
    
    public Deposito(Personas_Naturales clienteNatural, double capitalActual, float tiempoAniosLiquidez, int timepoDias, double interesRentabilidad) {
        super(clienteNatural);
        this.capitalActual = capitalActual;
        this.tiempoAniosLiquidez = tiempoAniosLiquidez;
        this.timepoDias = timepoDias;
        this.interesRentabilidad = interesRentabilidad;
    }
    public Deposito(Personas_Juridicas clienteJuridico, double capitalActual, float tiempoAniosLiquidez, int timepoDias, double interesRentabilidad) {
        super(clienteJuridico);
        this.capitalActual = capitalActual;
        this.tiempoAniosLiquidez = tiempoAniosLiquidez;
        this.timepoDias = timepoDias;
        this.interesRentabilidad = interesRentabilidad;
    }
  
    public Deposito(Personas_Naturales clienteNatural) {
        super(clienteNatural);
    }
    
    public Deposito(Personas_Juridicas clienteJuridico) {
        super(clienteJuridico);
    }
    
     public Deposito(Personas_Naturales clienteNatural, Deposito cliente) {
        super(clienteNatural);
        this.capitalActual = cliente.capitalActual;
        this.tiempoAniosLiquidez = cliente.tiempoAniosLiquidez;
        this.timepoDias = cliente.timepoDias;
        this.interesRentabilidad = cliente.interesRentabilidad;
    }
     
    public Deposito(Personas_Juridicas clienteJuridico, Deposito cliente) {
        super(clienteJuridico);
        this.capitalActual = cliente.capitalActual;
        this.tiempoAniosLiquidez = cliente.tiempoAniosLiquidez;
        this.timepoDias = cliente.timepoDias;
        this.interesRentabilidad = cliente.interesRentabilidad;
    }
    //metodos heredados
        @Override
    public double getBeneficio() { 
        return  liquidacion();
    }
    
    //metodos de la clase
    protected double liquidacion(){
        return getCapitalActual()*Math.pow(1+getInteresRentabilidad(),getTiempoAniosLiquidez());
    }
    
    public double getCapitalActual() {
        return capitalActual;
    }

    public void setCapitalActual(double capitalActual) {
        this.capitalActual = capitalActual;
    }

    public float getTiempoAniosLiquidez() {
        return tiempoAniosLiquidez;
    }

    public void setTiempoAniosLiquidez(int tiempoMesesLiquidez) {
        this.tiempoAniosLiquidez = tiempoMesesLiquidez;
    }

    public int getTimepoDias() {
        return timepoDias;
    }

    public void setTimepoDias(int timepoDias) {
        this.timepoDias = timepoDias;
    }

    public double getInteresRentabilidad() {
        return interesRentabilidad/100;
    }

    public void setInteresRentabilidad(double interesRentabilidad) {
        this.interesRentabilidad = interesRentabilidad;
    }

    @Override
    public String toString() {
        return 
                " Capital Actual:" + capitalActual + 
                " \n Periodo del prestamo en años " + tiempoAniosLiquidez +
                " \n Periodo del prestamo en dias " + timepoDias + 
                " \n tasa de interes " + interesRentabilidad +
                " \n liquidez " + (Math.round(liquidacion() * 100.0) / 100.0) +
                " \n Cobro de impuestos " + (Math.round(getImpuestos()* 100.0) / 100.0);
    }


    
    
}
