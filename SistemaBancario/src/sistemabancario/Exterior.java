/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemabancario;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author Black
 */
public class Exterior {
    
    //--inner class--
    private class Interna{
        public void imprimir() {
         System.out.println("---ESCUELA POLITECNICA NACIONAL---");
      }
    }
    // Acceso a la clase interna con el método
   void mostrar_Inner() {
      Interna inner = new Interna();
      inner.imprimir();
   }
   
   
   // Instanciar metodo de la clase exterior 
   void mi_Metodo() {
      final String proyecto = "OCJP : Sistema Bancario";
      //--method-local inner class--
      class MethodInner {
         public void print() {
            System.out.println("Proyecto de: "+proyecto);	   
         }   
      } // Fin del method-local inner class	 
      // Accessing the inner class
      MethodInner inner = new MethodInner();
      inner.print();
   }
   
   
   // método que acepta el objeto saludar de la interface amortizable
   public void mostrarMensaje(Bienvenida m) {
      System.out.println(m.saludar() +
         ": Castillo - Chasiliquin - Tinajero");
   }
   
   static class nested {
      public void mi_metodo() {
         System.out.println("--BIENVENIDOS--\n");
      }
   }
}
