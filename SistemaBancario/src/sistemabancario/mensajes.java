/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemabancario;

/**
 *
 * @author idenEclips
 */
public enum mensajes {
    ADVERTENCIATRANSAC("Advertencia: Se debe realizar esta transaccion con el monto de "),    
    TRANSACCIONPROGRESO("Transaccion en progreso"),
    TRANSACCIONCOMPLETA("Su cuenta ha sido acreditada"),
    CLIENTENOEXISTE("El cliente no existe para este tipo"),
    AMORTIZARRECHAZAR("El capital que desea retirar excede al actual"),
    AMORTIZAROK("Retiro del capital completo");
        
    private String name="";       
    mensajes(String mensaje){
        name = mensaje;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false 
        return name.equals(otherName);
    }

    @Override
    public String toString() {
       return this.name;
    }
    

}
