package sistemabancario;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class SistemaBancario {

    public static void main(String[] args) {
        FileWriter fichero = null;
        PrintWriter pw = null;       
        try
        {   

        
           
        // Instanciando la clase Exterior para Inner Class
        Exterior exterior = new Exterior();
        exterior.mostrar_Inner();

        //Instanciando la clase Exterior para Method-Local Inner Class
        Exterior outer = new Exterior();
        outer.mi_Metodo();

        // Instanciando la clase Exterior y Pasando una Anonymous Inner Class como argumento
        Exterior obj = new Exterior();
        obj.mostrarMensaje(new Bienvenida(){
            public String saludar(){
                return "Estudiantes";
            }
        });

        //Iniciando la Nested Class
        Exterior.nested nested = new Exterior.nested();	 
        nested.mi_metodo(); 
        
       //Imprimir Strings
        Strings prueba = new Strings();
        prueba.imprimirStrings();
        
            fichero = new FileWriter("../SistemaBancario/prueba.txt",true);
 
            pw = new PrintWriter(fichero);
        
        pw.println("PERSONA NATURAL");
        
        Integer edad= new Integer("24");
        Personas_Naturales personaNatural = new Personas_Naturales("1102145212","Maria","San Bartolo",edad.intValue(),"Soltera","Femenido");
        personaNatural.presentacionDatos();
                
        CuentaDeAhorros cuentaAhorrosNatural= new CuentaDeAhorros(124233114, personaNatural, 2000);
        pw.println("Datos de la cuenta "+cuentaAhorrosNatural.reintegro(600,"Natural"));
        
        
        Deposito nuevoClienteNatural = new Deposito(personaNatural, 20000 , (float) 0.5,50,3);
        pw.println("\nInformacion del deposito a plazo fijo\n"+nuevoClienteNatural.toString()) ;
        
        DepositoEstrcturado nuevoCapital = new DepositoEstrcturado(personaNatural,1000,0.3, nuevoClienteNatural);
        pw.println("\nInformacion del deposito a plazo variable\n"+nuevoCapital.toString()) ;
        boolean resp = nuevoCapital.amortizar(500);
        if (resp)
            pw.println(""+mensajes.AMORTIZAROK.toString());
                else
            pw.println(""+mensajes.AMORTIZARRECHAZAR.toString());
           // pw.println("\nInformacion del deposito a plazo variable\n"+nuevoCapital.toString()) ;
              

        } catch (Exception e) {
            System.out.println(""+e);
        }finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }

                        
        System.out.println("");
        Integer numSocios= new Integer(5);
        System.out.println("PERSONA JURIDICA\n");
        Personas_Juridicas personaJuridica = new Personas_Juridicas("12481222121","ProteCompu","La Patria",numSocios.intValue(),"Javier","05/12/2004");      
        personaJuridica.presentacionDatos();
        CuentaDeAhorros cuentaAhorrosJuridica= new CuentaDeAhorros(124233114, personaJuridica, 2000);
        Double deposito = new Double("600.50");
        System.out.println("Datos de la cuenta: "+cuentaAhorrosJuridica.ingresos(deposito.doubleValue(), "Juridica"));
        long retiro=500;
        System.out.println("Validación del retiro".toUpperCase());
        cuentaAhorrosJuridica.validacionRetiro(retiro,deposito);
        System.out.println("Retiro de dinero: "+cuentaAhorrosJuridica.reintegro(retiro, "Juridica")+"\n");
        
        
        Deposito nuevoClienteJuridico = new Deposito(personaJuridica);
        nuevoClienteJuridico.toString();
        numSocios=7;
        Personas_Juridicas personaJuridica1 = new Personas_Juridicas("1458795612","Firmesa","6 de Diciembre",numSocios.intValue(),"Pamela","04/12/2000");      
        personaJuridica1.presentacionDatos();
        CuentaCorriente cuentaCorrientaJuridica1= new CuentaCorriente(14579893, personaJuridica1, 5000, 1457);
        deposito=800.5;
        System.out.println("Datos de la cuenta "+cuentaCorrientaJuridica1.ingresos(deposito.doubleValue(), "Juridica"));
       
        
        Personas_Juridicas persJuri1Thread = new Personas_Juridicas("1722337712","Vivalca","Andaluz",5,"Jimmy","05/12/2017");      
        Personas_Juridicas persJuri2Thread = new Personas_Juridicas("1845562365","Olmedo","Faz",5,"Marlen","03/11/2017");   
        Personas_Juridicas persJuri3Thread = new Personas_Juridicas("1712154520","Irma","Perez",5,"Bonny","12/11/2017");     
        Personas_Juridicas persJuri4Thread = new Personas_Juridicas("1712556987","Calcedo","Remi",5,"Luis","22/09/2017");     
        
        ArrayList<Personas_Juridicas> perJuridica = new ArrayList<Personas_Juridicas>();
        perJuridica.add(personaJuridica);
        perJuridica.add(personaJuridica1);
        perJuridica.add(persJuri1Thread);
        perJuridica.add(persJuri2Thread);
        perJuridica.add(persJuri3Thread);
        perJuridica.add(persJuri4Thread);
        
        System.out.println("\nLas personas Juridicas son: ");
        mostrarDatosPJ(perJuridica);
        
        //hilos
        System.out.println("\tTHREADS");
        Hilos hilo1 = new Hilos("PERSONA JURIDICA 1");
        Hilos hilo2 = new Hilos("PERSONA JURIDICA 2");
        Hilos hilo3 = new Hilos("PERSONA JURIDICA 3");
        Hilos hilo4 = new Hilos("PERSONA JURIDICA 4");
  
        hilo1.setProcess(persJuri1Thread);
        hilo2.setProcess(persJuri2Thread);
        hilo3.setProcess(persJuri3Thread);
        hilo4.setProcess(persJuri4Thread);
 
        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
        
    }
    
    public static void mostrarDatosPJ (Collection c){ 
        Iterator<Personas_Juridicas> it = c.iterator();
	while (it.hasNext()) {
            System.out.print(it.next().nombre + ", ");
	}
	System.out.println("\n");
		
	}
    
    

    
    
}
