package sistemabancario;

public class Personas_Naturales extends Personas{
    //Atributos
    private int edad;
    private String estadoCivil;
    private String sexo;

    //Constructores
    public Personas_Naturales(String ci_O_ruc, String nombre, String direccion,int edad, String estadoCivil,String sexo) {
        super(ci_O_ruc, nombre, direccion);
        this.edad = edad;
        this.sexo = sexo;
        this.estadoCivil=estadoCivil;
    }
    
    public Personas_Naturales() {
        super();
    }
    
    //Metodos
    public int getEdad() {
        return edad;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public String getSexo() {
        return sexo;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }   
    
    public void presentacionDatos(){
        String datos="";
        datos+="Nombre: "+this.nombre+"\n";
        datos+="CI: "+this.ci_O_ruc+"\n";
        datos+="Direccion: "+this.direccion+"\n";
        datos+="Edad: "+this.edad+" años\n";
        datos+="Estado Civil: "+this.estadoCivil+"\n";
        datos+="Sexo: "+this.sexo+"\n";
        System.out.println(datos);
    }
  
}
