/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemabancario;

/**
 *
 * @author Black
 */
public class Strings {
    
        void imprimirStrings(){
            
        System.out.println("--Strings--");
        String cadena1 = "Demostracion de uso de Strings";
        String cadena2 = "PROBANDO STRINGS";
        System.out.println("Cadena1: "+cadena1+"\nCadena2: "+cadena2);
        
        //Longitud de cadenas
        System.out.println("La longitud de la cadena 1 es: "+cadena1.length());
        
        //Ubicacion de letra de cada cadena
        System.out.println("La primera letra de la cadena 2 es: "+cadena2.charAt(0));
        
        //Cadena 1 sin espacios en blanco
        System.out.println("Substring de la cadena 1 y cadena 2: "+cadena1.substring(0, 12)+cadena2.substring(0,8));
        
        //Cadena 1 a mayusculas y cadena 2 a minusculas
        System.out.println("Cadena 1 en mayusculas: "+cadena1.toUpperCase());
        System.out.println("Cadena 2 en minusculas: "+cadena2.toLowerCase());
        
        //comparacion de cadenas
        System.out.println("\nComparando cadena 1 y cadena 2:");
        if(cadena1.equals(cadena2)){
            System.out.println("Las cadenas son iguales");
        } else{
            System.out.println("Las cadenas NO son iguales");
        }
        
        //Que cadena es mas grande
        if(cadena1.compareTo(cadena2) == 0){
            System.out.println("Las cadenas son iguales");
        }
        else if(cadena1.compareTo(cadena2) < 0)
        {
            System.out.println("La cadena 1 es menor que la cadena 2");
        }
        else if(cadena1.compareTo(cadena2) > 0)
        {
            System.out.println("La cadena 1 es mayor que la cadena 2 ");
        }
        
        System.out.println("--Fin Strings--\n");
           
    }
    
}
