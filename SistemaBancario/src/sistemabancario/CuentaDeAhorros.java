package sistemabancario;

public class CuentaDeAhorros extends Cuenta{
    
    //Constructores
    public CuentaDeAhorros(long numCuenta, Personas_Juridicas titularPJ, double saldoActual) {
        super(numCuenta, titularPJ, saldoActual);
    }

    public CuentaDeAhorros(long numCuenta, Personas_Naturales titularPN, double saldoActual) {
        super(numCuenta, titularPN, saldoActual);
    }

    public CuentaDeAhorros() {
    }
    
    //Metodos
    @Override
    public String reintegro(double saldoAbonado, String tipoPersona) {
        return super.reintegro(saldoAbonado, tipoPersona); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void validacionRetiro(double montoRetiro,double saldoAbondao){
        assert montoRetiro <= this.saldoActual+saldoAbondao : "El monto del retiro debe ser menor o igual que el saldo actual";
        System.out.println("El monto del retiro es: " + montoRetiro+"\n");
    }
    @Override
    public String ingresos(double saldoAbonado, String tipoPersona) {
        return super.ingresos(saldoAbonado, tipoPersona); //To change body of generated methods, choose Tools | Templates.
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
}
